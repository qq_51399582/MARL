# 多智能体强化学习

介绍多智能体强化学习MARL相关算法，如 **IQL、VDN、QMIX、MADDPG、MATD3、MAPPO**等

多智能体深度强化学习（Multi-Agent Deep Reinforcement Learning，MADRL）是一类用于解决多智能体系统中决策与控制问题的算法。

多智能体深度强化学习将深度学习与多智能体强化学习结合，使得智能体能够在复杂、高维的环境中学习到有效的策略。MADRL 涉及多个智能体在共享环境中进行交互，这些智能体可能具有不同的目标、信息和能力，因此相较于单智能体强化学习问题，MADRL 更加复杂且具有挑战性。

# MADRL系列文章纲要：
MADRL系列主要针对多智能体深度强化学习相关算法进行介绍，并给出相关Pytorch代码和可移植程序，MADRL系列文章纲要如下：（持续更新中）
![image.png](https://raw.gitcode.com/qq_51399582/MARL/attachment/uploads/9666103a-af99-4c09-b1f1-8f19a07bfcea/image.png 'image.png')

### 1.【MADRL】多智能体深度强化学习《纲要》
https://rainbook.blog.csdn.net/article/details/141318848

### 2.【MADRL】独立Q学习（IQL）算法
https://rainbook.blog.csdn.net/article/details/141380210

### 3.【MADRL】基于MADRL的单调价值函数分解（QMIX）算法
https://rainbook.blog.csdn.net/article/details/141395534

### 4.【MADRL】多智能体深度确定性策略梯度（MADDPG）算法
https://rainbook.blog.csdn.net/article/details/141996518

### 5.【MADRL】多智能体双延迟深度确定性策略梯度（MATD3）算法
https://rainbook.blog.csdn.net/article/details/141997347

### 6.【MADRL】多智能体近似策略优化（MAPPO）算法
https://rainbook.blog.csdn.net/article/details/142068153

### 7.【MADRL】反事实多智能体策略梯度（COMA）算法
https://rainbook.blog.csdn.net/article/details/142109244

### 8.【MADRL】多智能体价值分解网络（VDN）算法 
https://rainbook.blog.csdn.net/article/details/142146917

### 9.【MADRL】多智能体信任域策略优化（MA-TRPO）算法 
https://rainbook.blog.csdn.net/article/details/142304128

### 10.【MADRL】面向角色的多智能体强化学习（ROMA）算法 
https://rainbook.blog.csdn.net/article/details/142338261

# MADRL配置：
python==3.11.5

torch==2.1.0

torchvision==0.16.0

gym==0.26.2

tensorboardX